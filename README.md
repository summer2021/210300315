# Web-based TDengine Management Toolkit【TDengine在线管理工具】
## 主要功能
- TDengine性能监控看板
- 数据的查询（分页、排序、分时）、导入、导出
- 数据库、超级表、表的树状结构展示
- 修改数据库设置、表结构
- 执行自定义SQL语句
- 简单的集群监控

## 前端构建
修改后台API地址及端口 public/config.js
### 开发

```bash
# 克隆项目
git clone https://gitlab.summer-ospp.ac.cn/summer2021/210300315.git

# 进入项目目录
cd 210300315/Web

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 修改后端接口配置public/config.js

# 启动服务
npm run dev
```

浏览器访问 http://localhost:9528

### 发布

```bash
# 构建生产环境
npm run build:prod
```
**将生成的dist放在/Web中，可以直接使用后端根路由来访问，也可以自己使用一些工具来部署，例如nginx。**

## 后端部署  
后端为Python3语言 ,将API代码部署在运行TDengine的机器上。

### 首先安装TDengine Python Connector
https://www.taosdata.com/cn/documentation/connector#python

```
# 克隆项目
git clone https://gitlab.summer-ospp.ac.cn/summer2021/210300315.git

# 进入后端文件夹下
cd 210300315/API

# 安装依赖库
pip install -r requirements.txt

# 修改后端代码的settings.py设置port

# 直接运行main文件
python main.py
```
### 后端目录文件结构

```
Project	
│  main.py	                后端入口
│  requirements.txt	        依赖
│  settings.py	                后台设置
│  TDglobal.py	                全局变量
├─core	后台核心
│  │  connect.py	        连接数据库
│  │  inherit_TD.py	        重写taos库部分功能
├─routers	                后台路由
│  │  database.py	        数据库操作路由
│  │  sql.py	                 Sql相关路由
│  │  table.py	                数据表操作路由
│  │  user.py	                用户相关路由
├─test	
│      data.csv	                导入数据测试文件
├─uploads	                导入数据上传目录
│      data.csv	
├─utils	工具
│  │  custom_exc.py	        自定义错误
│  │  response_code.py	        封装响应
│  │  security.py	        身份验证

```
## 界面截图
![img_6.png](img/img_6.png)
![img_1.png](img/img_1.png)
![img_2.png](img/img_2.png)
![img_3.png](img/img_3.png)
![img_4.png](img/img_4.png)
![img_5.png](img/img_5.png)