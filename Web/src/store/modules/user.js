import {login, logout, getInfo} from '@/api/user'
import {getToken, setToken, removeToken} from '@/utils/auth'
import {resetRouter} from '@/router'
import {switch_database} from "@/api/database";

const getDefaultState = () => {
  return {
    token: getToken(),
    name: '',
    avatar: ''
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  }
}

const actions = {
  // user login
  login(store, userInfo) {
    const {commit, dispatch, state, rootState} = store
    const {host, username, password, port} = userInfo

    return new Promise((resolve, reject) => {
      login({
        host: host,
        user: username.trim(),
        password: password,
        port: port,
        database: rootState.database.cur_database
      }).then(response => {
        const {data} = response
        commit('SET_TOKEN', data.token)
        setToken(data.token)
        console.log(rootState.database.cur_database)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },


  // login({commit}, userInfo) {
  //
  // },

  getInfo(store, config = {}) {
    //封装了一层 获取rootstate 来给其他module的state赋值
    const {commit, dispatch, state, rootState} = store
    return new Promise((resolve, reject) => {
      getInfo({database: rootState.database.cur_database}).then(response => {
        const {data} = response
        // const { rootState } = store
        if (!data) {
          return reject('Verification failed, please Login again.')
        }

        const {name, avatar} = data
        rootState.database.database_list = data[0]//来给database module的state赋值
        // commit('SET_NAME', name)
        // commit('SET_AVATAR', avatar)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })

  },

  // user logout
  logout
    ({commit, state}) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        removeToken() // must remove  token  first
        resetRouter()
        commit('RESET_STATE')
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  resetToken({commit}) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

