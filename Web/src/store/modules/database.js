const state = {
  database_list: [],
  cur_database: 'log',
  name: '',
  avatar: ''
}
import router from '../../router';

function page_reload() {
  router.push({path: '/dashboard'})
}

const mutations = {
  set_database: (state, name) => {
    state.cur_database = name
    setTimeout(page_reload, 800)
  },
}

const actions = {
  set_database({commit}, name) {
    console.log({commit}, name)
    commit('set_database', name)
  },

  // user login
  // login({ commit }, userInfo) {
  //   const { host, username, password } = userInfo
  //   return new Promise((resolve, reject) => {
  //     login({ host: host, user: username.trim(), password: password,database: }).then(response => {
  //       const { data } = response
  //       commit('SET_TOKEN', data.token)
  //       setToken(data.token)
  //       resolve()
  //     }).catch(error => {
  //       reject(error)
  //     })
  //   })
  // }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

