import request from '@/utils/request'

export function select_data(data) {
  return request({
    url: '/select_data',
    method: 'post',
    data
  })
}

export function add_field(data) {
  return request({
    url: '/add_field',
    method: 'post',
    data
  })
}


