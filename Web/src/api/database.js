import request from "@/utils/request";

export function switch_database(params) {
  return request({
    url: '/switch_database',
    method: 'get',
    params
  })
}
