import request from "@/utils/request";

export function execute_sql(data) {
  return request({
    url: '/execute_sql',
    method: 'post',
    data
  })
}

export function user_sql(data) {
  return request({
    url: '/user_sql',
    method: 'post',
    data
  })
}
