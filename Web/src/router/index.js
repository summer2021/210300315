import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: {title: '看板', icon: 'dashboard'}
    }]
  },
  {
    path: '/query',
    component: Layout,
    meta: {title: '数据查询', icon: 'form'},
    children: [
      {
        path: 'query_table',
        name: 'query_table',
        component: () => import('@/views/query/table'),
        meta: {title: '表数据查询', icon: 'form'}
      },
      {
        path: 'query_stable',
        name: 'query_stable',
        component: () => import('@/views/query/stable'),
        meta: {title: '超级表数据查询', icon: 'form'}
      }
    ]
  },
  {
    path: '/super_table',
    component: Layout,
    children: [
      {
        path: 'super_table',
        name: 'super_table',
        component: () => import('@/views/super_table/index'),
        meta: {title: '超级表管理', icon: 'form'}
      }
    ]
  },
  {
    path: '/tree',
    component: Layout,
    children: [
      {
        path: 'tree',
        name: 'tree',
        component: () => import('@/views/tree/index'),
        meta: {title: '树状视图', icon: 'form'}
      }
    ]
  },
  {
    path: '/sql',
    component: Layout,
    children: [
      {
        path: 'sql',
        name: 'sql',
        component: () => import('@/views/sql/index'),
        meta: {title: 'SQL执行', icon: 'form'}
      }
    ]
  },
  {
    path: '/nodes',
    component: Layout,
    children: [
      {
        path: 'nodes',
        name: 'nodes',
        component: () => import('@/views/nodes/index'),
        meta: {title: '集群信息', icon: 'form'}
      }
    ]
  },

  // 404 page must be placed at the end !!!
  {path: '*', redirect: '/404', hidden: true}
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({y: 0}),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
