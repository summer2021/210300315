import os

from core.inherit_TD import TDConnection


class GlobalV:
    person_online: bool
    DB_connection: TDConnection
    mount_path: str


def shutdown():
    g.person_online = False
    g.DB_connection.close()


g = GlobalV()
g.mount_path = os.getcwd()
