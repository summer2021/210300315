import os

import uvicorn
from fastapi import FastAPI, Depends
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import FileResponse

import TDglobal
from routers import user, database, table, sql
from utils.custom_exc import register_exception
from utils.security import check_jwt_token

app = FastAPI()
app = FastAPI(dependencies=[Depends(check_jwt_token)])
app.add_event_handler("shutdown", TDglobal.shutdown)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # 设置允许的origins来源
    allow_credentials=True,
    allow_methods=["*"],  # 设置允许跨域的http方法，比如 get、post、put等。
    allow_headers=["*"])  # 允许跨域的headers，可以用来鉴别来源等作用。

app.include_router(user.router, tags=['user'])
app.include_router(table.router, tags=['table'])
app.include_router(sql.router, tags=['sql'])
app.include_router(database.router, tags=['database'])


@app.get("/")
async def get_index():
    return FileResponse('../Web/dist/index.html')


@app.get("/{whatever:path}")
async def get_static_files_or_404(whatever):
    # try open file for path
    file_path = os.path.join("../Web/dist/", whatever)
    if os.path.isfile(file_path):
        return FileResponse(file_path)
    return FileResponse('public/index.html')


register_exception(app)

if __name__ == "__main__":
    import settings

    # mount_path = os.getcwd()
    uvicorn.run("main:app", host="0.0.0.0", port=settings.fastapi_port, reload=settings.fastapi_deload,
                debug=settings.fastapi_debug)
