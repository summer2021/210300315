from fastapi import APIRouter

from TDglobal import g
from utils import response_code

router = APIRouter()


@router.get("/switch_database")
async def get_info(database: str):
    c1 = g.DB_connection.cursor()
    c1.execute(f'USE {database};')
    c1.execute(f'SELECT DATABASE();')
    res = c1.fetchall_dict()
    return response_code.resp_200(data=res)
