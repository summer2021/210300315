from typing import Optional

from fastapi import APIRouter
from pydantic import BaseModel

from TDglobal import g
from utils import response_code

router = APIRouter()


class Item(BaseModel):
    sql: str
    fetchall: Optional[bool] = True


async def execute(item):
    data = {}
    c1 = g.DB_connection.cursor()
    c1.execute(item.sql)
    if item.fetchall:
        data = c1.fetchall_dict()
    c1.close()
    return data


@router.post("/execute_sql")
async def execute_sql(item: Item):
    data = await execute(item)
    return response_code.resp_200(data=data)


# app.exception_handler只能截取url所以就换个url
@router.post("/user_sql")
async def user_sql(item: Item):
    data = await execute(item)
    field = list(data[0].keys())
    return response_code.resp_200(data={
            'field': field,
            'data': data
        })