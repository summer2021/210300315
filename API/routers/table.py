from typing import Optional

from fastapi import APIRouter, File, UploadFile, Form
from pydantic import BaseModel

from TDglobal import g
from utils import response_code

router = APIRouter()


class Field_Item(BaseModel):
    name: str
    data_type: str
    add_type: str
    stable: str
    long: Optional[int] = None


class Select_Item(BaseModel):
    tname: str
    limit: int
    page: int
    sort: Optional[str] = ''
    time: Optional[list] = None


@router.post("/add_field")
async def add_field(item: Field_Item):
    cur = g.DB_connection.cursor()
    if item.data_type in ['BINARY', 'NCHAR']:
        item.data_type = f'{item.data_type}({item.long})'
    sql = f"ALTER STABLE {item.stable} ADD {item.add_type} {item.name} {item.data_type};"
    cur.execute(sql)
    return response_code.resp_200()


@router.post("/select_data")
async def select_data(item: Select_Item):
    cur = g.DB_connection.cursor()
    sql_plus = ''
    if item.time:
        sql_plus = f"AND ts > '{item.time[0]}'  and ts < '{item.time[1]}'"
    sql1 = f"SELECT count(*) FROM {item.tname} where _c0>0 {sql_plus}"
    cur.execute(sql1)
    total = cur.fetchall()
    if total == []:
        cur.execute(f'DESCRIBE {item.tname}')
        res = cur.fetchall_dict()
        return response_code.resp_200(data={
            'total': 0,
            'field': [i['Field'] for i in res],
            'data': []
        })
    offset = item.limit * (item.page - 1)  # 从哪开始
    sql2 = f'select * from {item.tname} where _c0>0 {sql_plus} ORDER BY ts {item.sort} limit {item.limit} offset {offset}'
    cur.execute(sql2)
    res = cur.fetchall_dict()
    field = list(res[0].keys())
    cur.close()
    return response_code.resp_200(data={
        'total': total[0][0],
        'field': field,
        'data': res
    })


@router.post("/input_file")
async def input_file(tname: str = Form(...), file: UploadFile = File(...)):
    cur = g.DB_connection.cursor()
    content = await file.read()
    with open(f'./uploads/{file.filename}', 'wb+') as f:
        f.write(content)
    cur.execute(f'insert into {tname} file "./uploads/{file.filename}";')
    return response_code.resp_200(data={'a': 'ok'})

# 只能用shell导出 改为纯前端导出
# @router.post("/export_file")
# async def export_file(tname: str = Form(...)):
#     os.system('taos')
# obj = subprocess.Popen(["taos"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
#                        shell=True,universal_newlines=True)
# obj.stdin.write(f'use test;')
# obj.stdin.write(f'select * from {tname} >> data.csv;')
# # obj.stdin.write(f'select * from {tname} >> data.csv;')
# # obj.stdin.write('exit')
# return FileResponse(file_name)
