from typing import Optional

from fastapi import APIRouter
from pydantic import BaseModel

from TDglobal import g
from core import connect
from utils import response_code
from utils.security import create_access_token

router = APIRouter()


class Item(BaseModel):
    host: str
    user: str
    password: str
    port: int
    database: Optional[str] = None


@router.post("/login")
async def check_users(item: Item):
    res = connect.get_connection(item)
    if res != 'connect to TDengine failed':
        g.DB_connection = res
        c1 = g.DB_connection.cursor()
        c1.execute(f'USE {item.database};')
        return response_code.resp_200(data={
            "token": create_access_token(item.user)})
    else:
        return response_code.resp_501(message=res)


@router.get("/info")
async def get_info(database: str):
    c1 = g.DB_connection.cursor()
    # if database != '':
    #     c1.execute(f'USE {database}')
    sql = ['SHOW DATABASES;', 'SELECT CLIENT_VERSION();',
           'SELECT SERVER_VERSION();']
    res = []
    for i in sql:
        c1.execute(i)
        data = c1.fetchall_dict()
        res.append(data)
    c1.close()
    for index, value in enumerate(res[0]):
        res[0][index]['created_time'] = str(value['created_time'])
    return response_code.resp_200(data=res)


@router.post("/logout")
async def logout():
    try:
        g.DB_connection.close()
    except:
        print('DB_connection.close error')
    return response_code.resp_200(data='success')
