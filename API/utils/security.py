from datetime import datetime, timedelta
from typing import Any, Union, Optional

from fastapi import Header
from jose import jwt
from starlette.requests import Request

import settings
from utils import custom_exc

# 导入配置文件

ALGORITHM = "HS256"


def create_access_token(subject: Union[str, Any]) -> str:
    """
    # 生成token
    :param subject: 保存到token的值
    :return:
    """
    expire = datetime.utcnow() + timedelta(
        minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES
    )  # 失效时间
    to_encode = {"exp": expire, "sub": str(subject)}
    encoded_jwt = jwt.encode(to_encode, settings.SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def check_jwt_token(request: Request, token: Optional[str] = Header(None)) -> Union[str, Any]:
    """
    解析验证 headers中为token的值 担任也可以用 Header(None, alias="Authentication") 或者 alias="X-token"
    :param request:
    :param token:
    :return:
    """
    # print(request.url.path)
    if request.url.path in ['/login', '/', '/config.js'] or request.url.path.split('/')[1] == 'static':  # 登录不校验token
        return None
    try:
        payload = jwt.decode(
            token,
            settings.SECRET_KEY, algorithms=[ALGORITHM]
        )
        return payload
    except (jwt.JWTError, jwt.ExpiredSignatureError, AttributeError):
        # 抛出自定义异常， 然后捕获统一响应
        raise custom_exc.UserTokenError(err_desc="access token fail")
