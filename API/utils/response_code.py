import json
from datetime import date, datetime
from typing import Union

from fastapi import status
from fastapi.responses import JSONResponse, Response  # , ORJSONResponse


class ComplexEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(obj, date):
            return obj.strftime('%Y-%m-%d')
        else:
            return json.JSONEncoder.default(self, obj)


class Response_plus(JSONResponse):
    def __init__(self, *args, **kwargs):
        super(Response_plus, self).__init__(*args, **kwargs)

    def render(self, content) -> bytes:
        return json.dumps(
            content,
            ensure_ascii=False,
            allow_nan=False,
            indent=None,
            separators=(",", ":"),
            cls=ComplexEncoder
        ).encode("utf-8")


def resp_200(*, data: Union[list, dict, str] = None, message: str = "Success"):
    return Response_plus(
        status_code=status.HTTP_200_OK,
        content={
            'code': 200,
            'message': message,
            'data': data,
        }
    )


def resp_501(*, data: Union[list, dict, str] = None, message: str = ""):
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content={
            'code': 501,
            'message': message,
            'data': data,
        }
    )


def resp_400(*, data: str = None, message: str = "BAD REQUEST") -> Response:
    return JSONResponse(
        status_code=status.HTTP_400_BAD_REQUEST,
        content={
            'code': 400,
            'message': message,
            'data': data,
        }
    )


def resp_403(*, data: str = None, message: str = "Forbidden") -> Response:
    return JSONResponse(
        status_code=status.HTTP_403_FORBIDDEN,
        content={
            'code': 403,
            'message': message,
            'data': data,
        }
    )


def resp_404(*, data: str = None, message: str = "Page Not Found") -> Response:
    return JSONResponse(
        status_code=status.HTTP_404_NOT_FOUND,
        content={
            'code': 404,
            'message': message,
            'data': data,
        }
    )


def resp_422(*, data: str = None, message: Union[list, dict, str] = "UNPROCESSABLE_ENTITY") -> Response:
    return JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content={
            'code': 422,
            'message': message,
            'data': data,
        }
    )


def resp_5002(*, data: str = None, message: Union[list, dict, str] = "TDengine Error") -> Response:
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content={
            'code': "5002",
            'message': message,
            'data': data,
        }
    )


# 自定义
def resp_50008(*, data: Union[list, dict, str] = None, message: str = "Token failure") -> Response:
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content={
            'code': 50008,
            'message': message,
            'data': data,
        }
    )


def resp_500(*, data: Union[list, dict, str] = None, message: str = "Token failure") -> Response:
    return JSONResponse(
        status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        content={
            'code': 500,
            'message': message,
            'data': data,
        }
    )


def resp_5001(*, data: Union[list, dict, str] = None, message: str = "User Not Found") -> Response:
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content={
            'code': 5001,
            'message': message,
            'data': data,
        }
    )
