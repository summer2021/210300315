from fastapi import FastAPI
from starlette.requests import Request
from taos.error import Error

from utils import response_code


class UserTokenError(Exception):
    def __init__(self, err_desc: str = "用户认证异常"):
        self.err_desc = err_desc


def register_exception(app: FastAPI):
    # 捕获全部异常
    @app.exception_handler(Exception)
    async def all_exception_handler(request: Request, exc: Exception):
        """
        全局所有异常
        """
        if request.scope['path'] == '/input_file':
            return response_code.resp_5002(data='可能文件字段不匹配')
        return response_code.resp_500(data='')

    # 自定义异常 捕获
    @app.exception_handler(UserTokenError)
    async def user_token_exception_handler(request: Request, exc: UserTokenError):
        """
        用户token异常
        """
        print('token error')
        return response_code.resp_50008(message=exc.err_desc)

    @app.exception_handler(Error)
    async def taos_exception_handler(request: Request, exc: Error):
        """
        taos错误拦截
        """
        print(exc.msg)
        if exc.msg == 'Invalid use of fetchall':
            if request.scope['path'] == '/user_sql':
                return response_code.resp_200(data='success and no fetchall', message='success and no fetchall')
            elif request.scope['path'] == '/execute_sql':
                return response_code.resp_200(data='success and no fetchall')
        else:
            print(exc.msg, request.scope['path'])
            if request.scope['path'] == '/user_sql':
                return response_code.resp_200(data=exc.msg, message='error')
            elif request.scope['path'] == '/execute_sql':
                return response_code.resp_5002(data=exc.msg)

    # @app.exception_handler(DatabaseError)
    # async def taos_exception_handler(request: Request, exc: Error):
    #     return response_code.resp_5002(data=exc.msg)

    # @app.exception_handler(ProgrammingError)
    # async def taos_exception_handler2(request: Request, exc: Error):
    #     return response_code.resp_5002(data=exc.msg)
