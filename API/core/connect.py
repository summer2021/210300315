from core.inherit_TD import TDConnection


def get_connection(item):
    try:
        conn = TDConnection(host=item.host, user=item.user, password=item.password, port=item.port, config="/etc/taos")
    except ConnectionError:
        return 'connect to TDengine failed'
    return conn


if __name__ == '__main__':
    item = {}
    get_connection(item)
