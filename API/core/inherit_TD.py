import taos


class TDConnection(taos.connection.TDengineConnection):
    def __init__(self, *args, **kwargs):
        super(TDConnection, self).__init__(*args, **kwargs)

    def cursor(self):
        return TDCursor(self)


class TDCursor(taos.cursor.TDengineCursor):
    def __init__(self, connection=None, *args, **kwargs):
        super(TDCursor, self).__init__(*args, **kwargs)
        if connection is not None:
            self._connection = connection

    def fetchall_dict(self):
        data = self.fetchall()
        fields = [i['name'] for i in self._fields]
        res = [dict(zip(fields, i)) for i in data]
        return res

    def execute(self, operation, params=None):
        print('Sql  '+operation)
        super(TDCursor, self).execute(operation)
